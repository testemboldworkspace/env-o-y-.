.. _glossary:

Glossary
========

.. glossary::

   Aggregated discovery service (ADS)
      ``NotDefined``

      .. note::

	 testing block inside term definition...

   Backend
      ``NotDefined``

   Bootstrap
      ``NotDefined``

   Circuit breaker
      ``NotDefined``

   Cluster
      ``NotDefined``

   Cluster discovery service (CDS)
      ``NotDefined``

   Control plane
      ``NotDefined``

   Data plane
      ``NotDefined``

   Delta update
      ``NotDefined``

   Downstream
      ``NotDefined``

   Dynamic configuration
      ``NotDefined``

   East-west traffic
      ``NotDefined``

   Edge proxy
      ``NotDefined``

   Egress
      ``NotDefined``

   Endpoint discovery service (EDS)
      ``NotDefined``

   Extensible discovery service (xDS)
      ``NotDefined``

   Extension
      ``NotDefined``

   Filter
      ``NotDefined``

   Front proxy
      ``NotDefined``

   gRPC
      ``NotDefined``

   HTTP connection manager (HCM)
      ``NotDefined``

   Ingress
      ``NotDefined``

   Listener
      ``NotDefined``

   Listener discovery service (LDS)
      ``NotDefined``

   Middle proxy
      ``NotDefined``

   Mutual TLS (mTLS)
      ``NotDefined``

   North-south traffic
      ``NotDefined``

   Route
      ``NotDefined``

   Route discovery service (RDS)
      ``NotDefined``

   Secrets discovery service (SDS)
      ``NotDefined``

   Service mesh
      ``NotDefined``

   Sidecar
      ``NotDefined``

   State of the world update
      ``NotDefined``

   Static configuration
      ``NotDefined``

   Transport layer security (TLS)
      ``NotDefined``

   Typed config
      ``NotDefined``

   Upstream
      ``NotDefined``

   Virtual host discovery service (VHDS)
      ``NotDefined``
